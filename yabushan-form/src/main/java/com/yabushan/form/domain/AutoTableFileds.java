package com.yabushan.form.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.yabushan.common.annotation.Excel;
import com.yabushan.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 动态字段信息对象 auto_table_fileds
 *
 * @author yabushan
 * @date 2021-08-06
 */
public class AutoTableFileds extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 字段ID */
    private String fId;

    /** 所属表ID */
    @Excel(name = "所属表ID")
    private String bId;

    /** 字段中文名称 */
    @Excel(name = "字段中文名称")
    private String filedCnName;

    /** 字段英文名称 */
    @Excel(name = "字段英文名称")
    private String filedEnNname;

    /** 字段类型，字典:FILED_TYPE */
    @Excel(name = "字段类型，字典:FILED_TYPE")
    private String filedType;

    /** 是否已删除 字典:sys_yes_no */
    @Excel(name = "是否已删除 字典:sys_yes_no")
    private String isDelete;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createdBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /** 更新人 */
    @Excel(name = "更新人")
    private String updatedBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;
    /** 表名称 */
    @Excel(name = "表名称")
    private String bCnName;

    public void setfId(String fId)
    {
        this.fId = fId;
    }

    public String getfId()
    {
        return fId;
    }
    public void setbId(String bId)
    {
        this.bId = bId;
    }

    public String getbId()
    {
        return bId;
    }
    public void setFiledCnName(String filedCnName)
    {
        this.filedCnName = filedCnName;
    }

    public String getFiledCnName()
    {
        return filedCnName;
    }
    public void setFiledEnNname(String filedEnNname)
    {
        this.filedEnNname = filedEnNname;
    }

    public String getFiledEnNname()
    {
        return filedEnNname;
    }
    public void setFiledType(String filedType)
    {
        this.filedType = filedType;
    }

    public String getFiledType()
    {
        return filedType;
    }
    public void setIsDelete(String isDelete)
    {
        this.isDelete = isDelete;
    }

    public String getIsDelete()
    {
        return isDelete;
    }
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }

    public Date getCreatedTime()
    {
        return createdTime;
    }
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }
    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }

    public Date getUpdatedTime()
    {
        return updatedTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("fId", getfId())
            .append("bId", getbId())
            .append("filedCnName", getFiledCnName())
            .append("filedEnNname", getFiledEnNname())
            .append("filedType", getFiledType())
            .append("isDelete", getIsDelete())
            .append("createdBy", getCreatedBy())
            .append("createdTime", getCreatedTime())
            .append("updatedBy", getUpdatedBy())
            .append("updatedTime", getUpdatedTime())
            .toString();
    }

    public String getbCnName() {
        return bCnName;
    }

    public void setbCnName(String bCnName) {
        this.bCnName = bCnName;
    }
}
