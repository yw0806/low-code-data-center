package com.yabushan.form.service;

import java.util.List;
import com.yabushan.form.domain.AutoUserTable;
import com.yabushan.form.domain.FiledInsertInfo;

/**
 *  动态表信息Service接口
 * 
 * @author yabushan
 * @date 2021-08-06
 */
public interface IAutoUserTableService 
{
    /**
     * 查询 动态表信息
     * 
     * @param bId  动态表信息ID
     * @return  动态表信息
     */
    public AutoUserTable selectAutoUserTableById(String bId);

    /**
     * 查询 动态表信息列表
     * 
     * @param autoUserTable  动态表信息
     * @return  动态表信息集合
     */
    public List<AutoUserTable> selectAutoUserTableList(AutoUserTable autoUserTable);

    /**
     * 新增 动态表信息
     * 
     * @param autoUserTable  动态表信息
     * @return 结果
     */
    public int insertAutoUserTable(AutoUserTable autoUserTable);

    /**
     * 修改 动态表信息
     * 
     * @param autoUserTable  动态表信息
     * @return 结果
     */
    public int updateAutoUserTable(AutoUserTable autoUserTable);

    /**
     * 批量删除 动态表信息
     * 
     * @param bIds 需要删除的 动态表信息ID
     * @return 结果
     */
    public int deleteAutoUserTableByIds(String[] bIds);

    /**
     * 删除 动态表信息信息
     * 
     * @param bId  动态表信息ID
     * @return 结果
     */
    public int deleteAutoUserTableById(String bId);

    public int addFiledValue(FiledInsertInfo filedInsertInfo);
}
