package com.yabushan.form.service;

import java.util.List;
import com.yabushan.form.domain.JimuReportDb;

/**
 * 报表APIService接口
 * 
 * @author yabushan
 * @date 2021-07-03
 */
public interface IJimuReportDbService 
{
    /**
     * 查询报表API
     * 
     * @param id 报表APIID
     * @return 报表API
     */
    public JimuReportDb selectJimuReportDbById(String id);

    /**
     * 查询报表API列表
     * 
     * @param jimuReportDb 报表API
     * @return 报表API集合
     */
    public List<JimuReportDb> selectJimuReportDbList(JimuReportDb jimuReportDb);

    /**
     * 新增报表API
     * 
     * @param jimuReportDb 报表API
     * @return 结果
     */
    public int insertJimuReportDb(JimuReportDb jimuReportDb);

    /**
     * 修改报表API
     * 
     * @param jimuReportDb 报表API
     * @return 结果
     */
    public int updateJimuReportDb(JimuReportDb jimuReportDb);

    /**
     * 批量删除报表API
     * 
     * @param ids 需要删除的报表APIID
     * @return 结果
     */
    public int deleteJimuReportDbByIds(String[] ids);

    /**
     * 删除报表API信息
     * 
     * @param id 报表APIID
     * @return 结果
     */
    public int deleteJimuReportDbById(String id);
}
