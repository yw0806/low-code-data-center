import request from '@/utils/request'

// 查询用户列表
export function listYmxuserInfo(query) {
  return request({
    url: '/ymx/ymxuserInfo/list',
    method: 'get',
    params: query
  })
}

// 查询用户详细
export function getYmxuserInfo(userId) {
  return request({
    url: '/ymx/ymxuserInfo/' + userId,
    method: 'get'
  })
}

// 新增用户
export function addYmxuserInfo(data) {
  return request({
    url: '/ymx/ymxuserInfo',
    method: 'post',
    data: data
  })
}

// 修改用户
export function updateYmxuserInfo(data) {
  return request({
    url: '/ymx/ymxuserInfo',
    method: 'put',
    data: data
  })
}

// 删除用户
export function delYmxuserInfo(userId) {
  return request({
    url: '/ymx/ymxuserInfo/' + userId,
    method: 'delete'
  })
}

// 导出用户
export function exportYmxuserInfo(query) {
  return request({
    url: '/ymx/ymxuserInfo/export',
    method: 'get',
    params: query
  })
}