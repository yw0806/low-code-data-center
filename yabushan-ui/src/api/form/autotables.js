import request from '@/utils/request'

// 查询用户建等级列表
export function listAutotables(query) {
  return request({
    url: '/form/autotables/list',
    method: 'get',
    params: query
  })
}

// 查询用户建等级详细
export function getAutotables(tId) {
  return request({
    url: '/form/autotables/' + tId,
    method: 'get'
  })
}

// 新增用户建等级
export function addAutotables(data) {
  return request({
    url: '/form/autotables',
    method: 'post',
    data: data
  })
}

// 修改用户建等级
export function updateAutotables(data) {
  return request({
    url: '/form/autotables',
    method: 'put',
    data: data
  })
}

// 删除用户建等级
export function delAutotables(tId) {
  return request({
    url: '/form/autotables/' + tId,
    method: 'delete'
  })
}

// 导出用户建等级
export function exportAutotables(query) {
  return request({
    url: '/form/autotables/export',
    method: 'get',
    params: query
  })
}