package com.yabushan.web.controller.system;

import java.util.List;

import com.yabushan.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.DataAliApi;
import com.yabushan.system.service.IDataAliApiService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 聚合数据Controller
 *
 * @author yabushan
 * @date 2021-08-08
 */
@RestController
@RequestMapping("/system/aliApi")
public class DataAliApiController extends BaseController
{
    @Autowired
    private IDataAliApiService dataAliApiService;

    /**
     * 查询聚合数据列表
     */
    @PreAuthorize("@ss.hasPermi('system:aliApi:list')")
    @GetMapping("/list")
    public TableDataInfo list(DataAliApi dataAliApi)
    {
        startPage();
        if(!"1".equals(dataAliApi.getIsGrant())){
            String username = SecurityUtils.getUsername();
            if(!username.equals("admin")){
                dataAliApi.setOwnerName(username);
            }
        }else{
            dataAliApi.setOwnerName("admin");
        }
        List<DataAliApi> list = dataAliApiService.selectDataAliApiList(dataAliApi);
        return getDataTable(list);
    }

    /**
     * 导出聚合数据列表
     */
    @PreAuthorize("@ss.hasPermi('system:aliApi:export')")
    @Log(title = "聚合数据", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DataAliApi dataAliApi)
    {
        String username = SecurityUtils.getUsername();
        if(!username.equals("admin")){
            dataAliApi.setCreatedBy(username);
        }
        List<DataAliApi> list = dataAliApiService.selectDataAliApiList(dataAliApi);
        ExcelUtil<DataAliApi> util = new ExcelUtil<DataAliApi>(DataAliApi.class);
        return util.exportExcel(list, "aliApi");
    }

    /**
     * 获取聚合数据详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:aliApi:query')")
    @GetMapping(value = "/{keyId}")
    public AjaxResult getInfo(@PathVariable("keyId") String keyId)
    {
        return AjaxResult.success(dataAliApiService.selectDataAliApiById(keyId));
    }

    /**
     * 新增聚合数据
     */
    @PreAuthorize("@ss.hasPermi('system:aliApi:add')")
    @Log(title = "聚合数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DataAliApi dataAliApi)
    {
        return toAjax(dataAliApiService.insertDataAliApi(dataAliApi));
    }

    /**
     * 修改聚合数据
     */
    @PreAuthorize("@ss.hasPermi('system:aliApi:edit')")
    @Log(title = "聚合数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DataAliApi dataAliApi)
    {
        return toAjax(dataAliApiService.updateDataAliApi(dataAliApi));
    }

    /**
     * 删除聚合数据
     */
    @PreAuthorize("@ss.hasPermi('system:aliApi:remove')")
    @Log(title = "聚合数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{keyIds}")
    public AjaxResult remove(@PathVariable String[] keyIds)
    {
        return toAjax(dataAliApiService.deleteDataAliApiByIds(keyIds));
    }
}
