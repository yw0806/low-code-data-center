package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.YmxInformationOptionsInfo;
import com.yabushan.system.service.IYmxInformationOptionsInfoService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 个人信息选项Controller
 *
 * @author yabushan
 * @date 2021-04-02
 */
@RestController
@RequestMapping("/ymx/ymxoptionsinfo")
public class YmxInformationOptionsInfoController extends BaseController
{
    @Autowired
    private IYmxInformationOptionsInfoService ymxInformationOptionsInfoService;

    /**
     * 查询个人信息选项列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxoptionsinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(YmxInformationOptionsInfo ymxInformationOptionsInfo)
    {
        startPage();
        List<YmxInformationOptionsInfo> list = ymxInformationOptionsInfoService.selectYmxInformationOptionsInfoList(ymxInformationOptionsInfo);
        return getDataTable(list);
    }

    /**
     * 导出个人信息选项列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxoptionsinfo:export')")
    @Log(title = "个人信息选项", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(YmxInformationOptionsInfo ymxInformationOptionsInfo)
    {
        List<YmxInformationOptionsInfo> list = ymxInformationOptionsInfoService.selectYmxInformationOptionsInfoList(ymxInformationOptionsInfo);
        ExcelUtil<YmxInformationOptionsInfo> util = new ExcelUtil<YmxInformationOptionsInfo>(YmxInformationOptionsInfo.class);
        return util.exportExcel(list, "ymxoptionsinfo");
    }

    /**
     * 获取个人信息选项详细信息
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxoptionsinfo:query')")
    @GetMapping(value = "/{optionsId}")
    public AjaxResult getInfo(@PathVariable("optionsId") String optionsId)
    {
        return AjaxResult.success(ymxInformationOptionsInfoService.selectYmxInformationOptionsInfoById(optionsId));
    }

    /**
     * 新增个人信息选项
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxoptionsinfo:add')")
    @Log(title = "个人信息选项", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody YmxInformationOptionsInfo ymxInformationOptionsInfo)
    {
        return toAjax(ymxInformationOptionsInfoService.insertYmxInformationOptionsInfo(ymxInformationOptionsInfo));
    }

    /**
     * 修改个人信息选项
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxoptionsinfo:edit')")
    @Log(title = "个人信息选项", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody YmxInformationOptionsInfo ymxInformationOptionsInfo)
    {
        return toAjax(ymxInformationOptionsInfoService.updateYmxInformationOptionsInfo(ymxInformationOptionsInfo));
    }

    /**
     * 删除个人信息选项
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxoptionsinfo:remove')")
    @Log(title = "个人信息选项", businessType = BusinessType.DELETE)
	@DeleteMapping("/{optionsIds}")
    public AjaxResult remove(@PathVariable String[] optionsIds)
    {
        return toAjax(ymxInformationOptionsInfoService.deleteYmxInformationOptionsInfoByIds(optionsIds));
    }
}
