package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.EmpSubForeignLanguage;
import com.yabushan.system.service.IEmpSubForeignLanguageService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 员工外国语子集Controller
 *
 * @author yabushan
 * @date 2021-03-21
 */
@RestController
@RequestMapping("/system/language")
public class EmpSubForeignLanguageController extends BaseController
{
    @Autowired
    private IEmpSubForeignLanguageService empSubForeignLanguageService;

    /**
     * 查询员工外国语子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:language:list')")
    @GetMapping("/list")
    public TableDataInfo list(EmpSubForeignLanguage empSubForeignLanguage)
    {
        startPage();
        List<EmpSubForeignLanguage> list = empSubForeignLanguageService.selectEmpSubForeignLanguageList(empSubForeignLanguage);
        return getDataTable(list);
    }

    /**
     * 导出员工外国语子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:language:export')")
    @Log(title = "员工外国语子集", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EmpSubForeignLanguage empSubForeignLanguage)
    {
        List<EmpSubForeignLanguage> list = empSubForeignLanguageService.selectEmpSubForeignLanguageList(empSubForeignLanguage);
        ExcelUtil<EmpSubForeignLanguage> util = new ExcelUtil<EmpSubForeignLanguage>(EmpSubForeignLanguage.class);
        return util.exportExcel(list, "language");
    }

    /**
     * 获取员工外国语子集详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:language:query')")
    @GetMapping(value = "/{recId}")
    public AjaxResult getInfo(@PathVariable("recId") String recId)
    {
        return AjaxResult.success(empSubForeignLanguageService.selectEmpSubForeignLanguageById(recId));
    }

    /**
     * 新增员工外国语子集
     */
    @PreAuthorize("@ss.hasPermi('system:language:add')")
    @Log(title = "员工外国语子集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpSubForeignLanguage empSubForeignLanguage)
    {
        return toAjax(empSubForeignLanguageService.insertEmpSubForeignLanguage(empSubForeignLanguage));
    }

    /**
     * 修改员工外国语子集
     */
    @PreAuthorize("@ss.hasPermi('system:language:edit')")
    @Log(title = "员工外国语子集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpSubForeignLanguage empSubForeignLanguage)
    {
        return toAjax(empSubForeignLanguageService.updateEmpSubForeignLanguage(empSubForeignLanguage));
    }

    /**
     * 删除员工外国语子集
     */
    @PreAuthorize("@ss.hasPermi('system:language:remove')")
    @Log(title = "员工外国语子集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recIds}")
    public AjaxResult remove(@PathVariable String[] recIds)
    {
        return toAjax(empSubForeignLanguageService.deleteEmpSubForeignLanguageByIds(recIds));
    }
}
